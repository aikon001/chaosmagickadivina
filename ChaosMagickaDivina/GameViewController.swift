//
//  GameViewController.swift
//  ChaosMagickaDivina
//
//  Created by marco luberto on 23/03/2020.
//  Copyright © 2020 marco luberto. All rights reserved.
//

import UIKit
import SceneKit
import SpriteKit
import GameplayKit



class GameViewController: UIViewController, SCNSceneRendererDelegate {

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Load 'GameScene.sks' as a GKScene. This provides gameplay related content
        // including entities and graphs.
        if let scene = main_Menu(fileNamed: "main_Menu") {
            
            // Get the SKScene from the loaded GKScene
            //if let sceneNode = scene.rootNode as! main_Menu? {
                
                // Copy gameplay related content over to the scene
                //sceneNode.entities = scene.entities
                //sceneNode.graphs = scene.graphs
                
                // Set the scale mode to scale to fit the window
                scene.scaleMode = .aspectFill
                
                // Present the scene
                if let view = self.view as! SKView? {
                    view.presentScene(scene)
                    
                    view.ignoresSiblingOrder = true
                    
                    //view.showsFPS = true
                    //view.showsNodeCount = true
                }
            }
        }
    

    override var shouldAutorotate: Bool {
        return true
    }

    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        if UIDevice.current.userInterfaceIdiom == .phone {
            return .allButUpsideDown
        } else {
            return .all
        }
    }

    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    
}
