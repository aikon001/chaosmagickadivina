//
//  GameScene.swift
//  ChaosMagickaDivina
//
//  Created by marco luberto on 23/03/2020.
//  Copyright © 2020 marco luberto. All rights reserved.
//

import SpriteKit
import SceneKit
import GameplayKit
import SceneKit.ModelIO

class GameScene: SKScene,SKPhysicsContactDelegate {
    
    var hp_item = SKSpriteNode(imageNamed: "hp_item")
    var mp_item = SKSpriteNode(imageNamed: "mp_item")
    var shield_item = SKSpriteNode(imageNamed: "shield_item")
    
    enum CategoryMask: UInt32 {
      case player = 0b01 // 1
      case bullet = 0b10 // 2
      case enemy = 0b11 // 3
    }
    var entities = [GKEntity]()
    var graphs = [String : GKGraph]()
    
    var timeSinceLastShoot = 0.00
    
    private var lastUpdateTime : TimeInterval = 0
    private var label : SKLabelNode?
    private var spinnyNode : SKShapeNode?
    
    let WitchSprite = SKSpriteNode()
    var MenuButton = SKSpriteNode()
    var SpellcardsSprite : [String:SKNode] = [:]
    var Inventory : [String:SKNode] = [:]
    var SelectedSpellcard = SKSpriteNode()
    var Shot = SKSpriteNode()
    var c = true
    var previousCardPos = CGPoint()
    var actualSpellcard = SKSpriteNode()
    var previousInvPos = CGPoint()
    var actualInv = SKSpriteNode()
    
    var HP : Float = 100
    var MP : Float = 0
    var Shield : Float = 0
    var HPBar = SKSpriteNode()
    var MPBar = SKSpriteNode()
    var ShieldBar = SKSpriteNode()
    var ShieldSprite = SKSpriteNode()
    
    //let bg = SKSpriteNode()
    var gamePaused = false
    var menu = SKShapeNode()
    var has_shield = false
    
    override func didMove(to view: SKView) {
        physicsWorld.contactDelegate = self
        physicsWorld.gravity = CGVector(dx: 0, dy: 0)
        let bundle = Bundle.main
        let path = bundle.path(forResource: "edo", ofType: "obj",inDirectory:"SceneAssets.scnassets")
        let url = NSURL(fileURLWithPath: path!)
        let asset = MDLAsset(url: url as URL)

        let scnScene = SCNScene(mdlAsset: asset)

        //scnScene.background.contents = GameScene.self
        let node = SK3DNode(viewportSize: CGSize(width: 800, height: 800))

        node.scnScene = scnScene
        //let sknode = node as SKNode
        //sknode.zPosition = -100
        node.zPosition = -100

        addChild(node)
        //addChild(bg)
        node.scnScene?.background.contents = UIImage(named: "sky")
        let rif = node.scnScene?.rootNode
        rif?.position.z += 1
        let rotation = SCNAction.rotateBy(x: .pi/16, y: .pi/12, z: 0, duration: 250)
        rif?.runAction(rotation)
        
        //node.scnScene?.rootNode.transform = SCNMatrix4MakeRotation(.pi/12, 1, 0, 0)
    }
    
    override func sceneDidLoad() {
        print("load")
        self.lastUpdateTime = 0
        
        // Loading witch with animation and physicsBody
        
        WitchSprite.size = CGSize(width: 64, height: 64)
        let WitchTextures = ArrayTexture(imageName: "witchsprite_idle",spritePerRow: 1,spritePerColumn: 4)
        WitchSprite.texture = WitchTextures[0]
        let IdleAnimation = SKAction.animate(with: WitchTextures, timePerFrame: 0.1)
        WitchSprite.xScale = -1.0 //flip horizontal
        addChild(WitchSprite)
        WitchSprite.run(SKAction.repeatForever(IdleAnimation))
        WitchSprite.physicsBody = SKPhysicsBody(rectangleOf: WitchSprite.size)
        WitchSprite.physicsBody?.affectedByGravity = false
        WitchSprite.physicsBody?.isDynamic  = false
        WitchSprite.physicsBody?.categoryBitMask = CategoryMask.player.rawValue
        WitchSprite.physicsBody?.contactTestBitMask = CategoryMask.enemy.rawValue
        WitchSprite.name = "witch"
        
        // Loading starting spellcards
        
        SpellcardsSprite["default"] = SKSpriteNode(imageNamed: "default_card")
        SpellcardsSprite["default"]?.position = CGPoint(x: -100, y: -120)
        SpellcardsSprite["fire"] = SKSpriteNode(imageNamed: "fire_card")
        SpellcardsSprite["fire"]?.position = CGPoint(x: -50, y: -120)
        SpellcardsSprite["water"] = SKSpriteNode(imageNamed: "water_card")
        SpellcardsSprite["water"]?.position = CGPoint(x: 0, y: -120)
        SpellcardsSprite["nocard1"] = SKSpriteNode(imageNamed: "no_card")
        SpellcardsSprite["nocard1"]?.position = CGPoint(x: 50, y: -120)
        SpellcardsSprite["nocard2"] = SKSpriteNode(imageNamed: "no_card")
        SpellcardsSprite["nocard2"]?.position = CGPoint(x: 100, y: -120)
        addChild(SpellcardsSprite["water"]!)
        addChild(SpellcardsSprite["fire"]!)
        addChild(SpellcardsSprite["default"]!)
        addChild(SpellcardsSprite["nocard1"]!)
        addChild(SpellcardsSprite["nocard2"]!)
        for spellcard in SpellcardsSprite{
            spellcard.value.zPosition = 1
        }
        
        // Loading selected spellcard
        
        SelectedSpellcard = SKSpriteNode(imageNamed: "card_selected")
        SelectedSpellcard.position = CGPoint(x: -100, y: -120)
        SelectedSpellcard.zPosition = 0
        addChild(SelectedSpellcard)
        
        // Loading start shot
        Shot = SKSpriteNode(imageNamed: "standardMagicaShot32x")
        
        
        //Loading inventory/menu button
        MenuButton = SKSpriteNode(imageNamed: "menubutton")
        MenuButton.position = CGPoint(x:-150,y: -120)
        MenuButton.zPosition = 1
        addChild(MenuButton)
        
        // Loading health UI
        HPBar = SKSpriteNode(imageNamed: "hpbar")
        HPBar.position = CGPoint(x: 15, y: 150)
        addChild(HPBar)
        MPBar = SKSpriteNode(imageNamed: "mpbar")
        MPBar.position = CGPoint(x: 15, y: 140)
        addChild(MPBar)
        ShieldBar = SKSpriteNode(imageNamed: "shieldbar")
        ShieldBar.position = CGPoint(x: 15, y: 130)
        addChild(ShieldBar)
        
        
    }
    
    
    func touchDown(atPoint pos : CGPoint) {
        let targetNode = atPoint(pos)
        if(!gamePaused){
            for spellcard in SpellcardsSprite{
                if targetNode == spellcard.value{
                    previousCardPos = targetNode.position
                    actualSpellcard = targetNode as! SKSpriteNode
                }
            }
        }else{
            for i in Inventory{
                if targetNode == i.value{
                    previousInvPos = targetNode.position
                    actualInv = targetNode as! SKSpriteNode
                }
            }
        }
        
    }
    
    func touchMoved(toPoint pos : CGPoint) {
        
       // let targetNode = atPoint(pos)
        if(!gamePaused){
        if(c){
            let movement = SKAction.move(to: pos, duration: 0.25)
            WitchSprite.run(movement)
            ShieldSprite.position = WitchSprite.position
        }
        for spellcard in SpellcardsSprite{
            if actualSpellcard == spellcard.value{
                spellcard.value.position = pos
                
            }
            
        }
        }else{
            for i in Inventory{
                if actualInv == i.value{
                    i.value.position = pos
                    
                }
            }
            
            
        }
    }
    
    func touchUp(atPoint pos : CGPoint) {
        //let targetNode = atPoint(pos)
        if(!gamePaused){
      
            for s in SpellcardsSprite{
                var key : String = ""
                if let index = SpellcardsSprite.values.firstIndex(of: actualSpellcard) {
                    key = SpellcardsSprite.keys[index]
                }
                if s.value != actualSpellcard && s.value.intersects(actualSpellcard){
                    generate (firstCard: key, secondCard: s.key)
                    break

                }
            }
        
        
        let movement = SKAction.move(to: previousCardPos, duration: 0.25)
        actualSpellcard.run(movement)
        actualSpellcard = SKSpriteNode()
        }else{
            
            for spellcard in SpellcardsSprite{
                var key : String = ""
                if actualInv.intersects(spellcard.value){
                    if let index = Inventory.values.firstIndex(of: actualInv) {
                        key = Inventory.keys[index]
                    }
                    
                    SpellcardsSprite[key] = actualInv
                    Inventory[spellcard.key] = spellcard.value
                    spellcard.value.removeFromParent()
                    
                    SpellcardsSprite[key]!.removeFromParent()
                    
                    SpellcardsSprite[key]!.position = spellcard.value.position
                    
                    addChild(SpellcardsSprite[key]!)
                    Inventory[key] = nil
                    SpellcardsSprite[spellcard.key] = nil
                    
                    menu.removeFromParent()
                    func_menu()
                    
                    actualInv = SKSpriteNode()
                }
            }
            
            let movementi = SKAction.move(to: previousInvPos, duration: 0.25)
            actualInv.run(movementi)
            actualInv = SKSpriteNode()
            
        }
        
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        for t in touches { self.touchDown(atPoint: t.location(in: self)) }
        
        let touch = touches.first as UITouch?
        let touchLocation = touch!.location(in: self)
        let targetNode = atPoint(touchLocation)
        
        if(!gamePaused){
        

        for spellcard in SpellcardsSprite{
            if targetNode == spellcard.value{
                c = false
                if spellcard.key != "nocard1" && spellcard.key != "nocard2"{
                    SelectedSpellcard.position = targetNode.position
                }
                
                switch spellcard.key{
                    
                case "default":
                    Shot = SKSpriteNode(imageNamed: "standardMagicaShot32x")
                
                case "water":
                    Shot = SKSpriteNode(imageNamed: "BubbleShot64x")
                
                case "fire":
                    let FireTextures = ArrayTexture(imageName: "FireballShot64x",spritePerRow: 4,spritePerColumn: 1)
                    let FireAnimation = SKAction.animate(with: FireTextures, timePerFrame: 0.1)
                    Shot.run(SKAction.repeatForever(FireAnimation))
                
                case "violet":
                    let FireTextures = ArrayTexture(imageName: "VioletballShot64x",spritePerRow: 4,spritePerColumn: 1)
                    let FireAnimation = SKAction.animate(with: FireTextures, timePerFrame: 0.1)
                    Shot.run(SKAction.repeatForever(FireAnimation))
                    
                case "ice":
                     Shot = SKSpriteNode(imageNamed: "ice_shot")
                    
                case "vapor":
                    let FireTextures = ArrayTexture(imageName: "vaporshot",spritePerRow: 1,spritePerColumn: 4)
                    let FireAnimation = SKAction.animate(with: FireTextures, timePerFrame: 0.1)
                    Shot.run(SKAction.repeatForever(FireAnimation))
                    
                    
                default:
                    break
                }
                
            }
            
        }
        
        if !(SpellcardsSprite.values.contains(targetNode)) {
            c = true
        }
            
        }
    
        if targetNode == MenuButton{
            gamePaused = !gamePaused
            
            if(gamePaused){
                menu = SKShapeNode(rectOf: CGSize(width: 200+(50*Inventory.count), height: 100))
                menu.fillColor = SKColor.brown
                menu.position.x = 0
                menu.zPosition =  1
                func_menu()
                
            }else{
                menu.removeFromParent()
            }
        }
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        for t in touches { self.touchMoved(toPoint: t.location(in: self)) }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        for t in touches { self.touchUp(atPoint: t.location(in: self)) }
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        for t in touches { self.touchUp(atPoint: t.location(in: self)) }
    }
    
    
    override func update(_ currentTime: TimeInterval) {
        // Called before each frame is rendered
        
        // Initialize _lastUpdateTime if it has not already been
        if (self.lastUpdateTime == 0) {
            self.lastUpdateTime = currentTime
        }
        
        // Calculate time since last update
        let dt = currentTime - self.lastUpdateTime
        
        // Update entities
        for entity in self.entities {
            entity.update(deltaTime: dt)
        }
        timeSinceLastShoot += lastUpdateTime
        if(!gamePaused){
        
            if timeSinceLastShoot > 0.25{
                Shoot()
                timeSinceLastShoot = 0
            }
            
            
            HPBar.size = CGSize(width: Int(HP*2), height: 20)
            MPBar.size = CGSize(width: Int(MP*2), height: 20)
            ShieldBar.size = CGSize(width: Int(Shield/2), height: 20)
            
            if Shield > 0 && !has_shield{
                ShieldSprite = SKSpriteNode(imageNamed: "Shield")
                ShieldSprite.position = WitchSprite.position
                addChild(ShieldSprite)
                ShieldSprite.zPosition = 3
                has_shield = true
            }else if Shield <= 0 {
                ShieldSprite.removeFromParent()
                has_shield = false
            }
        }
        
        
        self.lastUpdateTime = currentTime
        
        
    }
    
    func Shoot(){
        let shot = Shot.copy() as! SKSpriteNode
            
        shot.size = CGSize(width: Shot.size.width/2, height: Shot.size.height/2)
        shot.position = WitchSprite.position
        shot.physicsBody = SKPhysicsBody(circleOfRadius: shot.size.width/2)
        shot.physicsBody?.restitution = 0
        addChild(shot)
        shot.name = "bullet"
        shot.physicsBody?.applyForce(CGVector(dx: 800, dy: 0))
        let movement = SKAction.move(by: CGVector(dx:1000,dy:0), duration: 0.5)
        shot.run(movement)
        
        
    }
    
    func ArrayTexture(imageName:String,spritePerRow:Int,spritePerColumn:Int)->[SKTexture]{
        var Array : [SKTexture] = []
        let spriteSheetCutter = SpriteSheetCutter(
                    spriteImage: SKTexture(imageNamed: imageName),
                    spritePerRow: spritePerRow,
                    spritePerColumn: spritePerColumn)
        for i in 0...spriteSheetCutter.spritePerRow-1{
            for j in 0...spriteSheetCutter.spritePerColumn-1{
                Array.append(spriteSheetCutter.getSprite(spritePosition: CGPoint(x: i, y: j)))
            }
        }
        return Array
    }
    
    func PlayerDamage(damage:Float){
        if Shield > 0{
            Shield -= damage
        }else{
            HP -= damage
        }
    }
    
    func generate(firstCard:String,secondCard:String){
        var cardname : String = ""
        var filename : String = ""
        var nocard : String = ""
        var consume : Float = 0
        if (firstCard == "fire" && secondCard == "water") || (firstCard == "water" && secondCard == "fire"){
            cardname = "vapor"
            filename = "vapor_card"
            consume=25
        }
        
        else if (firstCard == "default" && secondCard == "fire") || (firstCard == "fire" && secondCard == "default"){
            cardname = "violet"
            filename = "VioletFire_card"
            consume=25
        }
        
        else if (firstCard == "default" && secondCard == "water") || (firstCard == "water" && secondCard == "default"){
            cardname = "ice"
            filename = "ice_card"
            consume=25
        }
        else
        {
            cardname = ""
        }
        if (consume <= MP){
            if !SpellcardsSprite.keys.contains(cardname) && !Inventory.keys.contains(cardname){
                if(cardname != ""){
                    show_popup(cardname: cardname)
                }
                if SpellcardsSprite["nocard1"] != nil || SpellcardsSprite["nocard2"] != nil{
                    MP -= consume
                    if(SpellcardsSprite["nocard1"] != nil){
                        nocard = "nocard1"
                    }else if(SpellcardsSprite["nocard2"] != nil){
                        nocard = "nocard2"
                    }
                    if(cardname != ""){
                        SpellcardsSprite[cardname] = SKSpriteNode(imageNamed: filename)
                        SpellcardsSprite[cardname]?.position = SpellcardsSprite[nocard]!.position
                        SpellcardsSprite[cardname]?.zPosition = 1
                        addChild(SpellcardsSprite[cardname]!)
                        SpellcardsSprite[nocard]?.removeFromParent()
                        SpellcardsSprite[nocard] = nil
                    }
                }else{
                    if(cardname != ""){
                        MP -= consume
                        Inventory[cardname] = SKSpriteNode(imageNamed: filename)
                    }
                }
            }
        }
    }
    
    func func_menu(){
        addChild(menu)
        var index : CGFloat = 0
        for i in Inventory{
            menu.addChild(i.value)
            i.value.position = CGPoint(x:menu.position.x+index-100,y:0)
            index += 50
        }
    }
    
    func show_popup(cardname:String){
        let body = SKShapeNode()
        body.position.y = -85
        addChild(body)
        let popup = SKAction.run {
            let rect = SKShapeNode(rectOf: CGSize(width: 500, height: 35))
            body.addChild(rect)
            let text = SKLabelNode(fontNamed: "Eight Bit Dragon")
            text.text = "New spell unlocked! " + cardname
            rect.addChild(text)
        }
        let removepopup = SKAction.fadeOut(withDuration: 5)
        self.run(popup)
        body.run(removepopup)
    }
    
    
  
}
