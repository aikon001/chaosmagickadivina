//
//  StageOne.swift
//  ChaosMagickaDivina
//
//  Created by marco luberto on 28/03/2020.
//  Copyright © 2020 marco luberto. All rights reserved.
//
import SpriteKit
import GameplayKit

class StageOne: GameScene {

    
    var enemies : [SKSpriteNode] = []
    var enemies_health : [String:Float] = [:]
    var casualTime : Float = Float.random(in: 10...15)
    var enemies_number = 0
    
    override func didMove(to view: SKView) {
        super.didMove(to: view)
    }
    
    override func sceneDidLoad() {
        super.sceneDidLoad()
        StageOneStart()
        //self.run(SKAction.wait(forDuration: 10))
        let spam = SKAction.run { self.spam_normal_demons(incrementPeriod: false) }
        let spam_big = SKAction.run { self.spam_big_demons() }
        let wait = SKAction.wait(forDuration: TimeInterval(casualTime))
        self.run(SKAction.repeat(SKAction.sequence([SKAction.wait(forDuration: TimeInterval(casualTime)),spam]), count: 8))
        let spam2 = SKAction.run { self.spam_normal_demons(incrementPeriod: true) }
        self.run(SKAction.sequence([SKAction.wait(forDuration: TimeInterval(10*casualTime)),SKAction.repeat(SKAction.sequence([wait,spam2,wait,spam_big]), count: 5)]))
    }
    
    override func update(_ currentTime: TimeInterval) {
        super.update(currentTime)
        for child in self.children{
            if child.name == "bullet"{
                child.physicsBody?.categoryBitMask = CategoryMask.bullet.rawValue
                child.physicsBody?.contactTestBitMask = CategoryMask.enemy.rawValue
            }
        }
        casualTime = Float.random(in: 15...35)
        
        for enemy in enemies{
            if enemy.position.x < -600{
                enemy.position.x += 500
            }
        }
    }
    
    func spam_big_demons(){
        print("spamming2")
        let enemyTexture = ArrayTexture(imageName: "ball_floating64x",spritePerRow: 6,spritePerColumn: 1)
        let enemyAnimation = SKAction.animate(with: enemyTexture, timePerFrame: 0.2)
        
        let spam = SKAction.run {
            self.enemies.append(SKSpriteNode())
        
            self.enemies[self.enemies_number].size = CGSize(width: 64, height: 64)
            self.enemies[self.enemies_number].run(SKAction.repeatForever(enemyAnimation))
            self.enemies[self.enemies_number].position = CGPoint(x: (10*self.enemies_number), y: (5*self.enemies_number)-50)
            self.addChild(self.enemies[self.enemies_number])
            let movedown = SKAction.move(by: CGVector(dx: 0, dy: 150), duration: 3.5)
            let moveup = SKAction.move(by: CGVector(dx: 0, dy: -150), duration: 3.5)
        self.enemies[self.enemies_number].run(SKAction.repeatForever(SKAction.sequence([movedown,moveup])))
        }
        
        self.run(spam)
    }
    
    func spam_normal_demons(incrementPeriod:Bool){
        
        let enemyTexture = ArrayTexture(imageName: "ball_floating64x",spritePerRow: 6,spritePerColumn: 1)
        let enemyAnimation = SKAction.animate(with: enemyTexture, timePerFrame: 0.2)
        
        var timeperiod : CGFloat = 1
        let spam = SKAction.run {
            self.enemies.append(SKSpriteNode())
        
            self.enemies[self.enemies_number].size = CGSize(width: 32, height: 32)
            self.enemies[self.enemies_number].run(SKAction.repeatForever(enemyAnimation))
            self.enemies[self.enemies_number].position = CGPoint(x: (32*self.enemies_number), y: (16*self.enemies_number)-50)
            self.addChild(self.enemies[self.enemies_number])
            
            let oscillate = SKAction.oscillation(amplitude: 90, timePeriod: timeperiod-0.5, midPoint: self.enemies[self.enemies_number].position)
            self.enemies[self.enemies_number].run(SKAction.repeatForever(oscillate))
            self.enemies[self.enemies_number].run(SKAction.repeatForever(SKAction.moveBy(x: -700, y: 0, duration: 5)))
            
            
            self.enemies[self.enemies_number].name = "enemy\(self.enemies_number)"
            self.enemies_health["enemy\(self.enemies_number)"] = 10
 
            self.enemies[self.enemies_number].physicsBody = SKPhysicsBody(circleOfRadius:16)
            self.enemies[self.enemies_number].physicsBody?.affectedByGravity = false
            self.enemies[self.enemies_number].physicsBody?.mass = 1000
            self.enemies[self.enemies_number].physicsBody?.restitution = 0
            
            self.enemies[self.enemies_number].physicsBody?.categoryBitMask = CategoryMask.enemy.rawValue
            self.enemies[self.enemies_number].physicsBody?.contactTestBitMask = CategoryMask.bullet.rawValue | CategoryMask.player.rawValue
            
            if(incrementPeriod){
                timeperiod += 1
            }
            
            self.enemies_number += 1
        }
        
        self.run(SKAction.repeat(spam, count: 10))
    }
    
    func didBegin(_ contact: SKPhysicsContact) {
        var enem = SKSpriteNode()
        var bulle = SKSpriteNode()
    
        if contact.bodyA.node != nil && contact.bodyB.node != nil{
            if (contact.bodyA.node!.name! == "bullet" && contact.bodyB.node!.name!.contains("enemy")) || (contact.bodyA.node!.name!.contains("enemy") && contact.bodyB.node!.name! == "bullet"){
                if contact.bodyA.node!.name! == "bullet" && contact.bodyB.node!.name!.contains("enemy"){
                    enem = contact.bodyB.node! as! SKSpriteNode
                    bulle = contact.bodyA.node! as! SKSpriteNode
                }else{
                    bulle = contact.bodyB.node! as! SKSpriteNode
                    enem = contact.bodyA.node! as! SKSpriteNode
                }
                print("hit")
                enem.physicsBody?.isDynamic = false
                enemies_health[enem.name!]! -= 4
                if enemies_health[enem.name!]! <= 0{
                    
                    generate_item(position:enem.position)
                    enem.removeFromParent()
                }
                bulle.removeFromParent()
                
            }
            
             if (contact.bodyA.node!.name! == "witch" && contact.bodyB.node!.name!.contains("enemy")) || (contact.bodyA.node!.name!.contains("enemy") && contact.bodyB.node!.name! == "witch"){
                print("collision")
                PlayerDamage(damage:5)
                
            }
            
            
            if (contact.bodyA.node!.name! == "witch" && contact.bodyB.node!.name!.contains("item") ) || (contact.bodyA.node!.name!.contains("item") && contact.bodyB.node!.name! == "witch" ){
                var item = SKSpriteNode()
                if contact.bodyB.node!.name!.contains("item"){
                    item = contact.bodyB.node! as! SKSpriteNode
                }else{
                    item = contact.bodyA.node! as! SKSpriteNode
                }
                if (contact.bodyA.node!.name! == "witch" && contact.bodyB.node!.name! == "hpitem" ) || (contact.bodyA.node!.name! == "hpitem" && contact.bodyB.node!.name! == "witch" ){
                    HP += 8.5
                }
                
                if (contact.bodyA.node!.name! == "witch" && contact.bodyB.node!.name! == "mpitem" ) || (contact.bodyA.node!.name! == "mpitem" && contact.bodyB.node!.name! == "witch" ){
                    MP += 7
                }
                if (contact.bodyA.node!.name! == "witch" && contact.bodyB.node!.name! == "shielditem" ) || (contact.bodyA.node!.name! == "shielditem" && contact.bodyB.node!.name! == "witch" ){
                    Shield += 8
                }
                item.removeFromParent()
            }
        }
        
    }
    
    func StageOneStart(){
        let body = SKShapeNode()
        body.zPosition = 2
        addChild(body)
        let popup = SKAction.run {
            let rect = SKShapeNode(rectOf: CGSize(width: 500, height: 35))
            body.addChild(rect)
            let text = SKLabelNode(fontNamed: "Eight Bit Dragon")
            text.text = "STAGE 1 : Blossom castle"
            rect.addChild(text)
        }
        let removepopup = SKAction.fadeOut(withDuration: 6.5)
        self.run(popup)
        body.run(removepopup)
    }
    
    func generate_item(position: CGPoint){
        let casualhp = Int.random(in: 0...8)
        let casualmp = Int.random(in: 0...5 )
        let casualshield = Int.random(in: 0...4)
        if casualhp <= 1{
            print("hp item")
            let hpitem = hp_item.copy() as! SKSpriteNode
            hpitem.physicsBody = SKPhysicsBody(rectangleOf: hpitem.size)
            hpitem.physicsBody?.restitution = 0
            hpitem.name = "hpitem"
            addChild(hpitem)
            hpitem.position = position
            let destroy = SKAction.run {hpitem.removeFromParent()}
            hpitem.run(SKAction.sequence([SKAction.wait(forDuration: 5),destroy]))
        }
        if casualmp <= 1{
            print("mp item")
            let mpitem = mp_item.copy() as! SKSpriteNode
            mpitem.physicsBody = SKPhysicsBody(rectangleOf: mpitem.size)
            mpitem.name = "mpitem"
            mpitem.physicsBody?.restitution = 0
            mpitem.position = position
            addChild(mpitem)
            let destroy = SKAction.run {mpitem.removeFromParent()}
            mpitem.run(SKAction.sequence([SKAction.wait(forDuration: 5),destroy]))
        }
        if casualshield <= 1{
            print("shield item")
            let shielditem = shield_item.copy() as! SKSpriteNode
            shielditem.physicsBody = SKPhysicsBody(rectangleOf: shielditem.size)
            shielditem.name = "shielditem"
            shielditem.physicsBody?.restitution = 0
            shielditem.position = position
            addChild(shielditem)
            let destroy = SKAction.run {shielditem.removeFromParent()}
            shielditem.run(SKAction.sequence([SKAction.wait(forDuration: 5),destroy]))
        }
    }
}

extension SKAction {
    static func oscillation(amplitude a: CGFloat, timePeriod t: CGFloat, midPoint: CGPoint) -> SKAction {
        let action = SKAction.customAction(withDuration: Double(t)) { node, currentTime in
            let displacement = a * sin(2 * .pi * currentTime / t)
            node.position.y = midPoint.y + displacement
        }

        return action
    }
}






