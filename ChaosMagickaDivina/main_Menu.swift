//
//  main_Menu.swift
//  ChaosMagickaDivina
//
//  Created by marco luberto on 28/03/2020.
//  Copyright © 2020 marco luberto. All rights reserved.
//
import SpriteKit
import GameplayKit

class main_Menu: SKScene {
    override func sceneDidLoad() {
        let bg = SKSpriteNode(imageNamed: "magic-cliff")
        bg.size = CGSize(width: 1350, height: 900)
        addChild(bg)
        bg.zPosition = -1
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        self.view?.presentScene(StageOne(fileNamed:"StageOne"))
        
    }
    
}
